class MittsController < ApplicationController
  before_action :set_mitt, only: [:show, :edit, :update, :destroy]

  # GET /mitts
  # GET /mitts.json
  def index
    @mitts = Mitt.all.order(created_at: :desc)
  end

  # GET /mitts/1
  # GET /mitts/1.json
  def show
  end

  # GET /mitts/new
  def new
    @mitt = Mitt.new
  end

  # POST /mitts
  def create
    @mitt = Mitt.new(mitt_params)
    @mitt.user = current_user
    if @mitt.save
      redirect_to timeline_url
    else
      render :new
    end
  end

  # DELETE /mitts/1
  def destroy
    @mitt.destroy
    flash[:success] = 'Mitt was successfully destroyed.'
    redirect_to timeline_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mitt
      @mitt = Mitt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mitt_params
      params.require(:mitt).permit(:content, :response_to_id)
    end
end
