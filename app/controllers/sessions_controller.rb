class SessionsController < ApplicationController
  def create
    user = User.find_by_username(session_params[:username])
    if user && user.authenticate(session_params[:password])
      session[:user_id] = user.id
      flash[:success] = "Welcome"
      redirect_to root_url
    else
      flash.now[:danger] = "Username or password don't match"
      render :new
    end
  end

  def new
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end

  private

    def session_params
      params.require(:session).permit(:username, :password)
    end
end
