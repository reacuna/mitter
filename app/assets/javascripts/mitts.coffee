# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

update_form_state = (length) ->
  mitt_form = $('form#new_mitt')
  group = $(mitt_form.find('.form-group'))
  textarea = $(mitt_form.find('textarea'))
  count = $(mitt_form.find('span.count'))
  button = $(mitt_form.find('input[type=submit]'))

  count.html(length)
  if length > 140
    button.prop("disabled", true)
    group.addClass('has-error')
  else
    button.prop("disabled", false)
    group.removeClass('has-error')


$ ->
  mitt_form = $('form#new_mitt')
  textarea = $(mitt_form.find('textarea'))
  update_form_state(textarea.val().length)
  textarea.keyup((event) ->
    chars = $(event.target).val().length
    update_form_state(chars)
  )
