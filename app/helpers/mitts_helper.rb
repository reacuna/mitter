module MittsHelper
  def link_usernames(text)
    text.gsub /@(\w+)/ do |username|
      user = User.where(username: username.gsub('@', '')).first
      if user != nil
        link_to(username, user_path(user))
      else
        username
      end
    end
  end
end
