# == Schema Information
#
# Table name: mitts
#
#  id             :integer          not null, primary key
#  content        :text
#  user_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  response_to_id :integer
#
# Indexes
#
#  index_mitts_on_response_to_id  (response_to_id)
#  index_mitts_on_user_id         (user_id)
#

class Mitt < ActiveRecord::Base
  belongs_to :user, inverse_of: :mitts
  has_many :responses, class_name: "Mitt", foreign_key: 'response_to_id'
  belongs_to :response_to, class_name: "Mitt"

  validates :content, length: { in: 1..140 }
  validates :user, presence: true

  def username
    return user.username unless user == nil
    return ''
  end

  def by_user?(test_user)
    user == test_user
  end

end
