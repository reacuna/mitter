require 'rails_helper'

RSpec.describe "mitts/new", type: :view do
  before(:each) do
    assign(:mitt, Mitt.new(
      :content => "MyText",
      :user => nil
    ))
  end

  it "renders new mitt form" do
    render

    assert_select "form[action=?][method=?]", mitts_path, "post" do

      assert_select "textarea#mitt_content[name=?]", "mitt[content]"

      assert_select "input#mitt_user_id[name=?]", "mitt[user_id]"
    end
  end
end
