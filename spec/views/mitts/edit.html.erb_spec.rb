require 'rails_helper'

RSpec.describe "mitts/edit", type: :view do
  before(:each) do
    @mitt = assign(:mitt, Mitt.create!(
      :content => "MyText",
      :user => nil
    ))
  end

  it "renders the edit mitt form" do
    render

    assert_select "form[action=?][method=?]", mitt_path(@mitt), "post" do

      assert_select "textarea#mitt_content[name=?]", "mitt[content]"

      assert_select "input#mitt_user_id[name=?]", "mitt[user_id]"
    end
  end
end
