require 'rails_helper'

RSpec.describe "mitts/index", type: :view do
  before(:each) do
    assign(:mitts, [
      Mitt.create!(
        :content => "MyText",
        :user => nil
      ),
      Mitt.create!(
        :content => "MyText",
        :user => nil
      )
    ])
  end

  it "renders a list of mitts" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
