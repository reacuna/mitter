require "rails_helper"

RSpec.describe MittsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/mitts").to route_to("mitts#index")
    end

    it "routes to #new" do
      expect(:get => "/mitts/new").to route_to("mitts#new")
    end

    it "routes to #show" do
      expect(:get => "/mitts/1").to route_to("mitts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/mitts/1/edit").to route_to("mitts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/mitts").to route_to("mitts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/mitts/1").to route_to("mitts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/mitts/1").to route_to("mitts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/mitts/1").to route_to("mitts#destroy", :id => "1")
    end

  end
end
