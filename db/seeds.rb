# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def mitt_content(users)
  content = Faker::StarWars.quote.truncate(130)
  mentions = users.sample(rand(0..2))
  words = content.split(' ')
  mentions.each do |user|
    words[rand(0..words.length - 1)] = "@#{user.username}"
  end
  content = words.join(' ')
end

User.create(username: 'reacuna', email: 'reacuna@miuandes.cl', password: 'hola')
Mitt.create(content: mitt_content([]), user: User.first, created_at: Faker::Time.backward(20))

20.times do
  username = Faker::Internet.user_name(nil, %w(_))
  User.create(username: username, email: Faker::Internet.safe_email(username), password: Faker::Internet.password)
end

users = User.all


400.times do
  previous_mitt = Mitt.all.sample
  mitt = Mitt.new(content: mitt_content(users), user: users.sample)
  mitt.created_at = Faker::Time.backward(14, :all)
  mitt.response_to = previous_mitt
  mitt.save
end
