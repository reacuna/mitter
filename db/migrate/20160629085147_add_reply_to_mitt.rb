class AddReplyToMitt < ActiveRecord::Migration
  def change
    add_reference :mitts, :response_to, index: true 
  end
end
