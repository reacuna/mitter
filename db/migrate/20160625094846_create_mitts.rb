class CreateMitts < ActiveRecord::Migration
  def change
    create_table :mitts do |t|
      t.text :content
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
