# == Route Map
#
#    Prefix Verb   URI Pattern               Controller#Action
#      root GET    /                         static_pages#index
#     login GET    /login(.:format)          sessions#new
#    signup GET    /signup(.:format)         users#new
#    logout DELETE /logout(.:format)         sessions#destroy
#  timeline GET    /timeline(.:format)       mitts#index
#     mitts POST   /mitts(.:format)          mitts#create
#  new_mitt GET    /mitts/new(.:format)      mitts#new
#      mitt GET    /mitts/:id(.:format)      mitts#show
#           DELETE /mitts/:id(.:format)      mitts#destroy
#     users POST   /users(.:format)          users#create
#  new_user GET    /users/new(.:format)      users#new
# edit_user GET    /users/:id/edit(.:format) users#edit
#      user GET    /users/:id(.:format)      users#show
#           PATCH  /users/:id(.:format)      users#update
#           PUT    /users/:id(.:format)      users#update
#           DELETE /users/:id(.:format)      users#destroy
#  sessions POST   /sessions(.:format)       sessions#create
#

Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  root 'mitts#index'

  get '/login', controller: :sessions, action: :new
  get '/signup', controller: :users, action: :new
  delete '/logout', controller: :sessions, action: :destroy
  get '/timeline', controller: :mitts, action: :index
  resources :mitts, except: [:edit, :update, :index]
  resources :users, except: [:index]
  resource :sessions, only: [:create]
end
